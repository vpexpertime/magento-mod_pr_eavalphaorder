<?php
namespace Pr\EavAlphaOrder\Plugin;

class LayeredNavEavOrder
{
    public function beforeSetPositionOrder(\Magento\Eav\Model\ResourceModel\Entity\Attribute\Option\Collection $subject, $dir, $sortAlpha = false)
    {
        return [$dir, $sortAlpha];
    }
}
